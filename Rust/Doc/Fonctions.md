## Création et appel d'une fonction
```rust
fn main(){
    ma_function();
}
fn ma_function(){
    println!("Hello");
}
```
Retour :  
`Hello`

---
## Les paramètres
```rust
fn main(){
    ma_function(50);
}
fn ma_function(x:i32){
    println!("{}", x);
}
```
Retour :  
`50`

---
## Les retours de fonction
```rust
fn main(){
    println!("{}",ma_function(50));
}
fn ma_function(x:i32) -> i32 {
    x * 2
    //OU ALORS
    return x * 2;
}
```
Retour :  
`100`  
**ATTENTION**
**Il ne faut pas mettre de `;` sinon aucun return ne sera pris en compte**

---
---
## Les boucles

### loop
`break` va stopper la boucle, la valeur après le break sera retournée 
```rust
fn main() {
    let mut compteur = 0;

    let resultat = loop {
        compteur += 1;

        if compteur == 10 {
            break compteur * 2;
        }
    };

    println!("Le résultat est {}", resultat);
}
```

---
# for
```rust
fn main() {
    let a = [10, 20, 30, 40, 50];

    for element in a {
        println!("La valeur est : {}", element);
    }
}
```
```rust
fn main() {
    for element in 0..5 {
        println!("La valeur est : {}", element);
    }
}
```