## Les mut
 
De base une variable est immuable

### Ne marche pas
```rust
fn main() {
    let x = 5;
    println!("La valeur de x est : {}", x);
    x = 6;
    println!("La valeur de x est : {}", x);
}
```
**Ici une erreur sera générée car _X_ est immuable**

---
### Marche 
```rust
fn main() {
    let mut x = 5;
    println!("La valeur de x est : {}", x);
    x = 6;
    println!("La valeur de x est : {}", x);
}
```
Ici `mut` rend une vatiable mutable

---
## Les types
### Entiers

| Taille | Signé | Non Signé | Mini Signé | Maxi Signé | Mini non Signé | Maxi non Signé |
|:----------:|:-------------:|:------:|:----:|:----:|:----:|:----:|
| 8 bits | i8 | u8 | -(2⁷) | 2⁷-1 | 0 | 2⁸-1 |
| 16 bits | i16 | u16 | -(2¹⁵) | 2¹⁵-1 | 0 | 2¹⁶-1 |
| 32 bits | i32 | u32 | -(2³¹) | 2³¹-1 | 0 | 2³²-1 |
| 64 bits | i64 | u64 | -(2⁶³) | 2⁷⁶³-1 | 0 | 2⁶⁴-1 |
| 128 bits | i128 | u128 | -(2¹²⁷) | 2¹²⁷-1 | 0 | 2¹²⁸-1|
| archi | isize | usize | | | |

---
### Flotants
|Type | Signé |
|:----:|:--:|
| binary32 | f32 |
| binary64 | f64 |

```rust
fn main() {
    let x:f64 = 5.2; //OK
    let x:u32 = 6; //OK
    let x:i32 = -6; //OK

    let x:u32 = -5 //NOK
    let x:i32 = 5.8 //NOK
}
```

### Autres

|Type | Symbole |
|:---:|:--:|
| boolean | bool |
| charactere | char |
| string | str |

---
### Les tuples
Les tuples permettent de regrouper plusieurs valeurs de type différent

```rust
fn main(){
    let tuple:(i32,f64,char) = (200,5.1,'a');
    let (x,y,z) = tuple;
    println!("La valeur de y est : {}", y);
}
```
```rust
fn main(){
    let tuple:(i32,f64,char) = (200,5.1,'a');
    println!("La valeur de la deuxieme valeur est : {}", tuple.1);
}
```
---
### Les tableaux 
```rust
fn main(){
    let a: [i32; 5] = [1, 2, 3, 4, 5];
    let premier = a[0];
}
```