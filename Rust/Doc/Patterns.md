## Déclaration de variable

On peut déclarer plusieurs variables de même type dans la même expression.
```rust
let (x,y) = (1,2);
```

---
## Match

Correspond a un switch case.

```rust
for i in 0..10{
	match i{
		0 => println!("0"),
		2 => println!("2"),
		4|5 => println!("4 ou 5"),
		_ => println!("Autre")
	}
}
```

---
## La liste des patterns 
[Doc Rust](https://doc.rust-lang.org/reference/patterns.html#identifier-patterns)