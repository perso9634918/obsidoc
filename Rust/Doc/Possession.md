## String
```rust
fn main(){
    let mut s = String::from("hello");

    s.push_str(", world!"); // push_str() ajoute un littéral de chaîne dans une String
    
    println!("{}", s); // Cela va afficher `hello, world!`
}
```
&str n'est pas mutable

---
## La portée
```rust
{
    let s = String::from("hello"); // s est en vigueur à partir de ce point
    
    // on fait des choses avec s ici
}                          // cette portée est désormais terminée, et s
                           // n'est plus en vigueur maintenant
```

---
### Déplacement

```rust
let s1 = String::from("hello");
```

Quand on assigne la string `hello` a **s1**

![alt](deplacement1.png)
```rust
println!("{}", s1);
```
`hello`

![alt](deplacement2.png)

```rust
let s2 = s1;
println!("{}",s2);
```
`hello`
```rust
println!("{}",s1);
```
Ici nous allons avoir une erreur car s1 à été déplacer vers s2

#### Comment contourner ça ?

```rust
    let s1 = String::from("hello");
    let s2 = s1.clone();
    println!("s1 = {}, s2 = {}", s1, s2);
```
`s1 = hello, s2 = hello`

---
## Possession & fonction
 
```rust
fn main(){
	let s = String::from("Hello"); //S rentre dans la portée
	let x = 5; //X rentre dans la portée
	
	copie_de_X(x);  //X va être déplacer dans la fonction mais 
					//i32 est copy donc il sera utilisable après
	println!("{}",x);
	
	possession_de_S(s);  //S va être déplacer dans la fonction
					     //String n'est copy donc il ne sera pas 
					     //utilisable après car il n'est plus dans la portée
	println!("{}",s);
}//X sort de la portée
//S sort de la portée mais comme il à été déplacer il ne se passe rien

fn possession_de_S(s: String){//S rentre dans la portée
	println!("{}",s);
}//Ici S sort de la portée 'drop' est appelé. La mémoire est free

fn copie_de_X(x: i32){//X rentre dans la portée
	println!("{}",x);
}//Ici X sort de la portée, rien ne se passe
```

```
https://jimskapt.github.io/rust-book-fr/ch04-01-what-is-ownership.html
```